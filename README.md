# Installing locally

* Checkout this repository
* Install Python and Node tools: `sudo apt install python3-dev python3-venv libpq-dev`
* Create a py3k virtualenv: `python3 -m venv ve`
* `ve/bin/python -m pip install wheel`
* Activate the virtualenv: `. ve/bin/activate`
* Install the requirements: `pip install -r requirements.txt`
* Create a `localsettings.py`: `cp localsettings.py.sample localsettings.py`
* Run migrations (creates the DB): `./manage.py migrate`
* Generate markdown page in DB: `./manage.py load_pages`
* Create the admin account: `./manage.py createsuperuser`
* Run the webserver: `./manage.py runserver`

# Filling out data

* `./manage.py wafer_add_default_groups` - Once-off, create auth groups.
  for registration.
* `./manage.py populate_sponsor_packages` - Once-off, create sponsorship
  packages.
* `./manage.py load_sponsors` - Load sponsors from git.

# FIXME (Missing data)
* `./manage.py populate_nights_meals` - Once-off, create meals & nights,

# Installing in production
* apt install
  - libjs-jquery
  - memcached
  - python3
  - python3-diff-match-patch
  - python3-django/buster-backports
  - python3-django-crispy-forms
  - python3-django-jsonfield
  - python3-django-nose
  - python3-django-reversion
  - python3-djangorestframework
  - python3-libravatar
  - python3-markdown
  - python3-pil
  - python3-psycopg2
  - python3-requests
  - python3-tz
  - python3-venv
  - python3-wheel
  - python3-yaml

# Also see

https://wiki.debconf.org/wiki/Wafer
